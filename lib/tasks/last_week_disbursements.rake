desc 'Calculates each merchant total disbursement for all orders   
      from the beginning to the end of the previous week'
task last_week_disbursements: :environment do
  date = Date.today.prev_occurring(:sunday).to_s

  WeeklyDisbursementWorker.perform_async(date)
end