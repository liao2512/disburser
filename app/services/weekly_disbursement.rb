class WeeklyDisbursement
  def initialize(date)
    date  = Time.parse(date)
    @from = date.beginning_of_week
    @to   = date.end_of_week
  end

  def calculate
    week_disbursable_orders.each_pair do |merchant_id, orders|
      Disbursement.calculate_by(merchant_id, orders, @to)
    end
  end

  private

    def week_disbursable_orders
      Order.where(completed_at: (@from..@to))
           .group_by(&:merchant_id)
    end
end