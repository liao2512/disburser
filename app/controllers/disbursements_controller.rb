class DisbursementsController < ApplicationController
  before_action :set_end_of_week
  before_action :set_merchant

  def index
    if @merchant
      disbursement = @merchant.disbursements.find_by(date: @end_of_week)
      @amount = disbursement ? disbursement.amount : '0.00'
    else
      @amount = Disbursement.week_total(@end_of_week)
    end
  end

  private

    def set_end_of_week
      date = params[:date] ? Date.parse(params[:date]) : Date.today.prev_occurring(:sunday)
      @end_of_week = date.end_of_week
    end

    def set_merchant
      @merchant = Merchant.find(params[:merchant_id]) if params[:merchant_id]
    end
end