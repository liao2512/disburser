json.set! :disbursement do
  json.set! :merchant_id, @merchant.id if @merchant
  json.set! :date, @end_of_week
  json.set! :amount, @amount
end