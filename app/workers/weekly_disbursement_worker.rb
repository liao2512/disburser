class WeeklyDisbursementWorker
  include Sidekiq::Worker

  def perform(date)
    WeeklyDisbursement.new(date).calculate
  end
end
