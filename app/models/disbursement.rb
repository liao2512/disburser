class Disbursement < ApplicationRecord
  belongs_to :merchant

  def self.calculate_by(merchant_id, orders, date)
    find_or_initialize_by(merchant_id: merchant_id, date: date).tap do |disbursement|
      disbursement.amount = orders.map(&:disbursement).sum
      disbursement.save!
    end
  end

  def self.week_total(date)
    from = date.beginning_of_week
    to   = date.end_of_week

    where(date: from..to).sum(:amount)
  end
end
