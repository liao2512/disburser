class Shopper < ApplicationRecord
  has_many :orders, dependent: :destroy

  validates :name, :email, :nif, presence: true
  validates :name, :email, length: { maximum: 120 }
  # TODO: add CustomValidator < ActiveModel::Validator for email and nif formats
end
