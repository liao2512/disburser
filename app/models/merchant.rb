class Merchant < ApplicationRecord
  has_many :orders,        dependent: :destroy
  has_many :disbursements, dependent: :destroy

  validates :name, :email, :cif, presence: true
  validates :name, :email, length: { maximum: 120 }
  # TODO: add CustomValidator < ActiveModel::Validator for email and cif formats
end
