class Order < ApplicationRecord
  SMALLER_THAN_50_FEE    = 0.01
  BETWEEN_50_AND_300_FEE = 0.0095
  OVER_300_FEE           = 0.0085

  scope :completed, -> { where.not(completed_at: [nil, '']) }

  belongs_to :merchant
  belongs_to :shopper

  validates :amount, presence: true
  validates :amount, numericality: { greater_than: 0 }

  def disbursement
    amount * (1 - disbursement_fee)
  end

  private

    def disbursement_fee
      case
      when amount < 50  then SMALLER_THAN_50_FEE
      when amount > 300 then OVER_300_FEE
      else
        BETWEEN_50_AND_300_FEE
      end
    end
end