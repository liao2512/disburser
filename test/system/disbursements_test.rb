require "application_system_test_case"

class DisbursementsTest < ApplicationSystemTestCase
  setup do
    @disbursement = disbursements(:one)
  end

  test "visiting the index" do
    visit disbursements_url
    assert_selector "h1", text: "Disbursements"
  end

  test "creating a Disbursement" do
    visit disbursements_url
    click_on "New Disbursement"

    fill_in "Amount", with: @disbursement.amount
    fill_in "Date", with: @disbursement.date
    fill_in "Merchant", with: @disbursement.merchant_id
    click_on "Create Disbursement"

    assert_text "Disbursement was successfully created"
    click_on "Back"
  end

  test "updating a Disbursement" do
    visit disbursements_url
    click_on "Edit", match: :first

    fill_in "Amount", with: @disbursement.amount
    fill_in "Date", with: @disbursement.date
    fill_in "Merchant", with: @disbursement.merchant_id
    click_on "Update Disbursement"

    assert_text "Disbursement was successfully updated"
    click_on "Back"
  end

  test "destroying a Disbursement" do
    visit disbursements_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Disbursement was successfully destroyed"
  end
end
