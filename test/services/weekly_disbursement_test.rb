require 'test_helper'

class WeeklyDisbursementTest < ActiveSupport::TestCase
  test "calculate" do
    assert_difference('Disbursement.count', 1) do
      WeeklyDisbursement.new('2019-02-01').calculate
    end
    
    last_disbursement = Disbursement.last
    
    assert_equal merchants(:one), last_disbursement.merchant
    assert_equal 396.48, last_disbursement.amount
  end
end