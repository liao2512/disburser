require 'test_helper'

class DisbursementsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @disbursement = disbursements(:one)
    @merchant = merchants(:one)
  end

  test "should get last week total disbursements" do
    get disbursements_url, as: :json
    assert_response :success

    actual_json = JSON.parse(@response.body)
    expected_json = {"disbursement"=>{"date"=>"2019-03-24", "amount"=>"701.98"}}
    assert_equal expected_json, actual_json
  end

  test "should return merchant disbursements if existant" do
    get merchant_disbursements_url(@merchant, date: '2019-01-01'), as: :json
    assert_response :success
 
    actual_json = JSON.parse(@response.body)
    expected_json = {"disbursement"=>{"merchant_id"=>@merchant.id, "date"=>"2019-01-06", "amount"=>"100.99"}}
    assert_equal expected_json, actual_json
  end
  
  test "should return 0 if merchant disbursements does not exist" do
    get merchant_disbursements_url(@merchant, date: '2010-01-01'), as: :json
    assert_response :success
 
    actual_json = JSON.parse(@response.body)
    expected_json = {"disbursement"=>{"merchant_id"=>@merchant.id, "date"=>"2010-01-03", "amount"=>"0.00"}}
    assert_equal expected_json, actual_json
  end
end