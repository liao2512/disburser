require 'test_helper'

class OrderTest < ActiveSupport::TestCase
  test 'disbursement when order under 50' do
    order = orders(:under_50)

    # TODO: determine logic to round numbers with +2 decimals
    assert_equal 49.4901, order.disbursement
  end

  test 'disbursement when order between 50 and 300' do
    order = orders(:in_50_300)

    assert_equal 49.525, order.disbursement
  end

  test 'disbursement when order over 300' do
    order = orders(:over_300)

    assert_equal 297.459915, order.disbursement
  end
end