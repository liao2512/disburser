# README

This application calculates how much money should be disbursed to each merchant based on a set of rules.

## SETUP

To set up this application make sure you have PostgreSQL and Redis installed and run:

`bundle install`

`rails db:setup`

`bundle exec sidekiq`

`rails test`

`rails server`

On production, set the following cron to calculate the previous week disbursements:

`0 1 * * 1 cd /app/root/path && /path/to/bundle exec rake last_week_disbursements >> /path/to/logs/cron.log 2>&1`


## INSTRUCTIONS

- `/disbursements.json?date=yyyy-mm-dd` gets a given week total (all merchants) disbursement.

- `/merchants/:id/disbursements.json?date=yyyy-mm-dd` gets a single merchant given week total disbursement.

- `params[:date]` can be any day belonging to the given week

- If no `params[:date]` is given, it will calculate the previous week from the current day. 


## ASUMPTIONS, TRADEOFFS and TODOS

- A merchant disbursement for a given week takes all completed orders from monday 00:00:00 to sunday 23:59:59.

- Since many databsae queries are done searching by dates (`completed_at`), we need table indexes on those column for better performance.

- Tests fixtures urgently need proper naming.

- `reduce(:+)` seems faster than `sum` in my console, but less legible. I traded off speed for readability.
 
- I am recalcualting the total weekly disbursement everytime it is requested. It's perhaps better to persist this record on the database.

- DisbursementsController#index is too fat.

- Testing for the rake task and sidekiq worker are pending TODO.

- Unit test for records validations, an authentication logic and index paginations are missing, among many other things.

- Maybe there are too many commits. They could be squash on a interactive rebase into more meaningful chunks.