puts 'seeding Merchants...'
merchants_json = ActiveSupport::JSON.decode(File.read('db/seeds/merchants.json'))
merchants_json["RECORDS"].each do |merchant|
  Merchant.find_or_create_by(merchant)
end

puts 'seeding Shoppers...'
shoppers_json  = ActiveSupport::JSON.decode(File.read('db/seeds/shoppers.json'))
shoppers_json["RECORDS"].each do |shopper|
  Shopper.find_or_create_by(shopper)
end

puts 'seeding Orders...'
orders_json    = ActiveSupport::JSON.decode(File.read('db/seeds/orders.json'))
orders_json["RECORDS"].each do |order|
  Order.find_or_create_by(order)
end

puts 'seeding Disbursements...'
orders_by_merchant = Order.completed.group_by(&:merchant_id)

orders_by_merchant.each_pair do |merchant_id, orders|
  orders_by_end_of_week = orders.group_by{ |order| order.completed_at.end_of_week.to_date }

  orders_by_end_of_week.each_pair do |end_of_week, orders|
    Disbursement.calculate_by(merchant_id, orders, end_of_week)
  end
end