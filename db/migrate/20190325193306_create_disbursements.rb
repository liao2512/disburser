class CreateDisbursements < ActiveRecord::Migration[5.2]
  def change
    create_table :disbursements do |t|
      t.decimal :amount, precision: 8, scale: 2, null: false
      t.date :date, null: false
      t.references :merchant, foreign_key: true

      t.timestamps
    end
  end
end
