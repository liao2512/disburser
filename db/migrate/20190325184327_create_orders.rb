class CreateOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :orders do |t|
      t.decimal :amount, precision: 8, scale: 2, null: false
      t.datetime :completed_at
      t.references :merchant, foreign_key: true
      t.references :shopper, foreign_key: true

      t.timestamps
    end
  end
end
