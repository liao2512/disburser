Rails.application.routes.draw do
  require 'sidekiq/web'
  mount Sidekiq::Web => '/sidekiq'

  resources :shoppers
  resources :orders
  resources :merchants do
    resources :disbursements, only: :index
  end
  resources :disbursements, only: :index

  root 'merchants#index'
end
